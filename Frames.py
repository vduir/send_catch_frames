#!/bin/python3
"""
Данный скрипт передает кадры с разным битрейтом (для косвенного управлять модуляцией)
Синтаксис такой - ./Frames.py <диапазон> <ширина канала>
Например, для передачи кадров в диапазоне 2.4ГГц с шириной канала 20 МГц - ./Frames.py 2 20
./Frames.py d 20 - постоянно шлет кадры с битрейтом  1 Мб/с для детектирования источника излучения
"""

from scapy.all import *
from scapy.layers.dot11 import *
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('r', type=str, help="range 2.4Ghz, 5.8Ghz or 'd' (d == 2.4 GHz for DETECTION)")
parser.add_argument('b', type=str, help='bandwidth 20/40 MHz')
args = parser.parse_args()

dst_mac = 'ff:ff:ff:ff:ff:ff'
src_mac = '11:11:11:11:11:11'
ssid = 'Scapy_AP'
interface = 'wlx1'
channel_2Ghz = 1
channel_5Ghz = 36

inter = 0.5
frame_count = 10

rate_list_bg = [1, 2, 5.5, 11, 6, 9, 12, 18, 24, 36, 48, 54]    # 2.4 GHz bg
rate_list_ag = [6, 9, 12, 18, 24, 36, 48, 54]       # 5.8a GHz ag
rate_HT_20 = [6.5, 13, 19.5, 26, 39, 52, 58.5, 65]    # 2.4 | 5.8 GHz n  HT 20Mhz
rate_HT_40 = [13.5, 27, 40.5, 54, 81, 108, 121.5, 135]    # 2.4 | 5.8 GHz n  HT 40Mhz

def check_fr_range(value):
    if value == '2':
        return value
    elif value == '5':
        return value
    elif value == 'd':
        return 'd'
    else:
        print('Wrong range! It must be 2 or 5 Mhz')
        exit(0)


def check_bandwidth(value):
    if value == '20':
        value = 'HT20'
        return value
    elif value == '40':
        value = 'HT40+'
        return value
    else:
        print('Wrong bandwidth! it must be 20 or 40')
        exit(0)


dot11 = Dot11(type=0, subtype=8, addr1=dst_mac, addr2=src_mac, addr3=src_mac)
beacon = Dot11Beacon(cap="ESS+privacy")
ssid = Dot11Elt(ID='SSID', info=ssid, len=len(ssid))

def send_frame_detact():
    os.system(f'sudo iw dev {interface} set channel {channel_2Ghz} HT20')
    frame = RadioTap(
        present='Rate',
        Rate=1,
    ) / dot11 / beacon / ssid
    sendp(frame, iface=interface, loop=1, inter=inter)

def send_bg_frame():
        os.system(f'sudo iw dev {interface} set channel {channel_2Ghz} {bandwidth}')
        frame = RadioTap(
            present='Rate',
            Rate=rate_list_bg,
        ) / dot11 / beacon / ssid
        wrpcap("example.pcap", frame)
        sendp(frame, iface=interface, count=frame_count, inter=inter)

def send_ag_frame():
        os.system(f'sudo iw dev {interface} set channel {channel_5Ghz} {bandwidth}')
        frame = RadioTap(
            present='Rate',
            Rate=rate_list_ag,
        ) / dot11 / beacon / ssid
        wrpcap("example.pcap", frame)
        sendp(frame, iface=interface, count=frame_count, inter=inter)


# def send_g_frame():
#     os.system(f'sudo iw dev {interface} set channel {channel_2Ghz} HT20')
#     frame = RadioTap(
#         present='Rate',
#         Rate=rate_list_ag,
#     ) / dot11 / beacon / ssid
#     wrpcap("example.pcap", frame)
#     sendp(frame, iface=interface, count=frame_count, inter=inter)


def send_HT20_frame(channel):
        os.system(f'sudo iw dev {interface} set channel {channel} {bandwidth}')
        frame = RadioTap(
            present='MCS+VHT',
            knownMCS='MCS_index',
            MCS_index=range(7),
            KnownVHT='STBC+Bandwidth+GroupID+PartialAID',
            PresentVHT='STBC',
            VHT_bandwidth=0,
            GroupID=1,
            PartialAID=1
        ) / dot11 / beacon / ssid
        wrpcap("example.pcap", frame)
        sendp(frame, iface=interface, count=frame_count, inter=inter)


def send_HT40_frame(channel):
    os.system(f'sudo iw dev {interface} set channel {channel} {bandwidth}')
    frame = RadioTap(
        present='MCS+VHT',
        knownMCS='MCS_index',
        MCS_index=range(7),
        KnownVHT='STBC+Bandwidth+GroupID+PartialAID',
        PresentVHT='STBC',
        VHT_bandwidth=3,
        GroupID=1,
        PartialAID=1
    ) / dot11 / beacon / ssid
    wrpcap("example.pcap", frame)
    sendp(frame, iface=interface, count=frame_count, inter=inter)


# пока что получается запустить VHT с  битрейтов в 6Mb/s что не особо похоже на AC стандарт

# def send_VHT_frame(channel):
#         os.system(f'sudo iw dev {interface} set channel {channel} 80MHz')
#         frame = RadioTap(
#             version=0,
#             pad=0,
#             # present='Flags+Channel+dBm_AntSignal+Lock_Quality+RXFlags+RadiotapNS+Ext+VHT',
#             present='Flags+Channel+dBm_AntSignal+Lock_Quality+Ext+VHT',
#             # Flags='FCS',
#             ChannelFrequency=5220,
#             ChannelFlags='Dynamic_CCK_OFDM+5GHz',
#             dBm_AntSignal=-10,
#             Lock_Quality=63,
#
#             Ext=b'\x20\x08\x00\xa0\x20\x08\x00\x00',
#             notdecoded=b'\x00\x00\xcc\x01',
#             RXFlags=b'',
#             KnownVHT='STBC+Bandwidth+GroupID+GuardInterval+PartialAID',
#             VHT_bandwidth=2,
#             GroupID=0,
#             PartialAID=0
#         ) / dot11 / beacon / ssid
#         wrpcap("example.pcap", frame)
#         sendp(frame, iface=interface, loop=1, inter=inter)




if __name__ == '__main__':
    fr_range = check_fr_range(args.r)
    bandwidth = check_bandwidth(args.b)

    os.system('rfkill unblock all')
    os.system('ifconfig wlx1 up')


    if fr_range == 'd' and bandwidth == 'HT20':
        print(f'DETECTION | {channel_2Ghz} channel | {bandwidth} MHz | b')
        send_frame_detact()
    elif fr_range == 'd' and bandwidth == 'HT40+':
        print(f'DETECTION | {channel_2Ghz} channel | {bandwidth} MHz | b')
        send_frame_detact()
    elif fr_range == '2' and bandwidth == 'HT20':
        print(f'{channel_2Ghz} channel | {bandwidth} MHz | bgn ')
        send_bg_frame()
        send_HT20_frame(channel_2Ghz)
    elif fr_range == '2' and bandwidth == 'HT40+':
        print(f'{channel_2Ghz} channel | {bandwidth} MHz | bgn ')
        send_bg_frame()
        send_HT40_frame(channel_2Ghz)
    elif fr_range == '5' and bandwidth == 'HT20':
        print(f'{channel_5Ghz} channel | {bandwidth} MHz | agn ')
        send_ag_frame()
        send_HT20_frame(channel_5Ghz)
    elif fr_range == '5' and bandwidth == 'HT40+':
        print(f'{channel_5Ghz} channel | {bandwidth} MHz | agn ')
        send_ag_frame()
        send_HT40_frame(channel_5Ghz)
    else:
        print('Something wrong')
        exit(0)