#!/bin/bash

# скрипт для захвата дампа на всех адаптерах устройства с указанием условного места источника сигнала, его диапазона и ширины канала
# синтаксис такой ./Catch_frames.sh <место_источника> <частотный_диапазон> <ширина_канала>, например:
#  ./Catch_frames.sh point_1 2 20       - скрипт выставит адаптеры на диапазон 2.4ГГц (1й канал по умолчанию) с шириной канала 20 МГц
#										  и  запишет файл дампа с соответствующим именем


trap ' 
killall tcpdump; 
echo tcpdump stopped!;
for iface in ${ifaces[@]}
do
	iw dev ${iface} set channel 1 HT20
done
exit
' SIGINT

place=$1
fr_range=$2
bandwidth=$3
ifaces=("wlx1" "wlx2" "wlx3" "wlx4")
cirlce=('|'' '/'' '--' '\')
# mac=0C:80:63:AA:4F:1E
mac='11:11:11:11:11:11'


systemctl stop reedpipe		# на случай если забыли погасить пипу

# перевод интерфейсов в соответствующий диапазон частот
if [[ $fr_range == 2 ]] && [[ $bandwidth == 20 ]]; then
	for iface in ${ifaces[@]}
	do
		iw dev $iface set channel 1 HT20
	done
elif [[ $fr_range == 5 ]] && [[ $bandwidth == 20 ]]; then
	for iface in ${ifaces[@]}
	do
		iw dev $iface set channel 36 HT20
	done
elif [[ $fr_range == 2 ]] && [[ $bandwidth == 40 ]]; then
	for iface in ${ifaces[@]}
	do
		iw dev $iface set channel 1 HT40+
	done
elif [[ $fr_range == 5 ]] && [[ $bandwidth == 40 ]]; then
	for iface in ${ifaces[@]}
	do
		iw dev $iface set channel 36 HT40+
	done
else
	echo 'Wrong range! It must be 2GHz or 5GHz'
	exit 0
fi

mkdir ./Recorded
mkdir ./Recorded/${place}

# запуск записи дампа
for iface in ${ifaces[@]}
do
	tcpdump -i $iface wlan addr2 ${mac} -w ./Recorded/${place}/${place}_${iface}_${fr_range}_$bandwidth.pcap &
done

# цикл для отображения процесса работы скрипта
while true
do
	for var in ${cirlce[@]}
	do
		clear
		echo  recording dump on ${fr_range}GHz range [$var]
		wc -c ./Recorded/${place}/*
		sleep 0.5
	done
done