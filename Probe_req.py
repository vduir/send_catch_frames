#!/bin/python3
from scapy.all import *
from scapy.layers.dot11 import *


recipients_mac_adress = 'ff:ff:ff:ff:ff:ff'
your_mac_adress = '00:c0:ca:a8:a8:89'
ssid = 'Scapy_Probe_req'
channel = chr(11)
interface = 'wlx_1'

frame = RadioTap() \
        / Dot11(type=0, subtype=4, addr1=recipients_mac_adress, addr2=your_mac_adress, addr3=recipients_mac_adress) \
        / Dot11ProbeReq() \
        / Dot11Elt(ID='SSID', info=ssid) \
        / Dot11Elt(ID='Rates', info='\x82\x84\x8b\x96\x0c\x12\x18') \
        / Dot11Elt(ID='ESRates', info='\x30\x48\x60\x6c') \
        / Dot11Elt(ID='DSset', info=channel)
answer = sendp(frame, iface=interface, loop=1, inter=1)